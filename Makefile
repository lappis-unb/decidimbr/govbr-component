build:
	docker-compose build

up:
	docker-compose up -d

attach:
	docker exec -it lappis_ws_decidim_server bash

stop:
	docker-compose stop
