# frozen_string_literal: true

require "decidim/components/namer"

Decidim.register_component(:initial_page) do |component|
  component.engine = Decidim::InitialPage::Engine
  component.admin_engine = Decidim::InitialPage::AdminEngine
  component.icon = "decidim/initial_page/icon.svg"

  # component.on(:before_destroy) do |instance|
  #   # Code executed before removing the component
  # end

  # These actions permissions can be configured in the admin panel
  # component.actions = %w()

  component.settings(:global) do |settings|
    settings.attribute :description, type: :string, default: ""
  end

  # component.settings(:step) do |settings|
  #   # Add your settings per step
  # end

  component.register_resource(:initial_page) do |resource|
    resource.model_class_name = "Decidim::InitialPage::Page"
    # resource.template = "decidim/initial_page/some_resources/linked_some_resources"
  end

  # component.register_stat :some_stat do |context, start_at, end_at|
  #   # Register some stat number to the application
  # end

  # component.seeds do |participatory_space|
  #   # Add some seeds for this component
  # end
end
