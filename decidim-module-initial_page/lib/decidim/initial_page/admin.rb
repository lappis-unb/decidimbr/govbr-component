# frozen_string_literal: true

module Decidim
  module InitialPage
    # This module contains all the domain logic associated to Decidim's InitialPage
    # component admin panel.
    module Admin
    end
  end
end
