# frozen_string_literal: true

require "rails"
require "decidim/core"

module Decidim
  module InitialPage
    # This is the engine that runs on the public interface of initial_page.
    class Engine < ::Rails::Engine
      isolate_namespace Decidim::InitialPage

      routes do
        # Add engine routes here
        # resources :initial_page
        root to: "initial_page#index"
      end

      initializer "InitialPage.webpacker.assets_path" do
        Decidim.register_assets_path File.expand_path("app/packs", root)
      end
    end
  end
end
