# frozen_string_literal: true

module Decidim
  module InitialPage
    # This is the engine that runs on the public interface of `InitialPage`.
    class AdminEngine < ::Rails::Engine
      isolate_namespace Decidim::InitialPage::Admin

      paths["db/migrate"] = nil
      paths["lib/tasks"] = nil

      routes do
        root to: "initial_page#index"
      end

      def load_seed
        nil
      end
    end
  end
end
