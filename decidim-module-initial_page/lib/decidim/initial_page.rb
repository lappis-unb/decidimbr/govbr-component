# frozen_string_literal: true

require "decidim/initial_page/admin"
require "decidim/initial_page/engine"
require "decidim/initial_page/admin_engine"
require "decidim/initial_page/component"

module Decidim
  # This namespace holds the logic of the `InitialPage` component. This component
  # allows users to create initial_page in a participatory space.
  module InitialPage
  end
end
