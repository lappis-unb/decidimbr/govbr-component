class CreateInitialPage < ActiveRecord::Migration[6.1]
  def change
    create_table :initial_pages do |t|
      t.string :description
      t.timestamps
    end
  end
end
