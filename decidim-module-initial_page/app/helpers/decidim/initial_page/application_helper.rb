# frozen_string_literal: true

module Decidim
  module InitialPage
    # Custom helpers, scoped to the initial_page engine.
    #
    module ApplicationHelper
    end
  end
end
