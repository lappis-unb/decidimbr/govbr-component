module Decidim
  module InitialPage
    class Page < ApplicationRecord
      include Decidim::HasComponent
      # def initialize(*args)
      # end

      def self.log_presenter_class_for(_log)
        Decidim::Pages::AdminLog::PagePresenter
      end

      def title
        component.name
      end

      def self.create(component_instance)
        @page = Page.new(
          description: component_instance.description,
          component: component_instance
        )
        @page.save!
      end
    end
  end
end
