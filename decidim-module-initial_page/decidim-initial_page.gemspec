# frozen_string_literal: true

$LOAD_PATH.push File.expand_path("lib", __dir__)

require "decidim/initial_page/version"

Gem::Specification.new do |s|
  s.version = Decidim::InitialPage.version
  s.authors = [""]
  s.email = [""]
  s.license = "AGPL-3.0"
  s.homepage = "https://github.com/decidim/decidim-module-initial_page"
  s.required_ruby_version = ">= 3.0"

  s.name = "decidim-initial_page"
  s.summary = "A decidim initial_page module"
  s.description = "Show inital page for participatory process."

  s.files = Dir["{app,config,lib}/**/*", "LICENSE-AGPLv3.txt", "Rakefile", "README.md"]

s.add_dependency "decidim-core", "0.28.0.dev"
end
